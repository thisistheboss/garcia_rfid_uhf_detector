/*
 * TIM1_PWM.h
 *
 *  Created on: 13.02.2019
 *      Author: johan
 */

#ifndef TIM1_PWM_H_
#define TIM1_PWM_H_

void TIM1_init (void);
void BUZZER_on (void);
void BUZZER_off (void);

#endif /* TIM1_PWM_H_ */
